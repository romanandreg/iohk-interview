# Change log

otp-test uses [Semantic Versioning][1].
The change log is available [on GitHub][2].

[1]: http://semver.org/spec/v2.0.0.html
[2]: https://github.com/githubuser/otp-test/releases

## v0.0.0.1

* Initially created.
