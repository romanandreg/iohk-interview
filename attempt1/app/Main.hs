{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE NoImplicitPrelude #-}
module Main where

import RIO
import Data.Text.Encoding (decodeUtf8)

import qualified System.Etc as Etc
import qualified Control.Distributed.Backend.P2P as P2P
import qualified Control.Distributed.Process as DP
import qualified Control.Distributed.Process.Node as DPN
import Data.FileEmbed (embedFile)

import Primitives
import Lib
import Util

getConfig :: IO Etc.Config
getConfig = do
  configSpec <-
    Etc.parseConfigSpec (decodeUtf8 $(embedFile "resources/config.spec.yml"))

  let configDef = Etc.resolveDefault configSpec
  configCli <- Etc.resolvePlainCli configSpec
  (configFile, _fileWarnings) <- Etc.resolveFiles configSpec
  configEnv <- Etc.resolveEnv configSpec

  return (configDef <> configEnv <> configCli <> configFile)

main :: IO ()
main = do
  config <- getConfig

  host           <- Etc.getConfigValue ["host"] config
  (port :: Int)  <- Etc.getConfigValue ["port"] config
  otherHosts     <- Etc.getConfigValue ["other-hosts"] config
  withSeed       <- Etc.getConfigValue ["with-seed"] config
  sendForSeconds <- Etc.getConfigValue ["send-for"] config
  waitForSeconds <- Etc.getConfigValue ["wait-for"] config

  let portStr = show port
      fullHost = host <> ":" <> portStr
      otherHosts1 = filter (/= fullHost) otherHosts

  let
    startSystem =
      bootstrapNonBlocking
        host
        (show port)
        (const (host, show port))
        DPN.initRemoteTable
        (map P2P.makeNodeId otherHosts1)
        (do threadDelay 100000
            runDistributedSystem withSeed sendForSeconds waitForSeconds)

  bracket startSystem
          (\(node, _, closeTransport) -> do
             closeTransport
             DPN.closeLocalNode node
             logErr "Server Done"
          )
          (\_ ->
              threadDelay $ (sendForSeconds + waitForSeconds) * 1000100
          )
