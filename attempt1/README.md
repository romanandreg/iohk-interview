# otp-test

> Some quote here

## Table Of Contents

* [Raison d'etre](#raison-detre)
* [Documentation](#documentation)
* [Installation](#installation)
* [Development](#development)

## Raison d'etre

Give me a reason to care

## Documentation

Documentation can be found [here](https://romanandreg.gitbooks.io/otp-test/content/)

## Installation

[![Hackage](https://img.shields.io/hackage/v/otp-test.svg)](https://img.shields.io/hackage/v/otp-test.svg)
[![Stackage LTS](https://www.stackage.org/package/otp-test/badge/lts)](http://stackage.org/lts/package/otp-test)
[![Stackage Nightly](https://www.stackage.org/package/otp-test/badge/nightly)](http://stackage.org/nightly/package/otp-test)

Make sure you include the following entry on your [cabal file's
dependecies](https://www.haskell.org/cabal/users-guide/developing-packages.html#build-information)
section.

```cabal
library:
  build-depends: ...
                 , otp-test
```

Or on your `package.yaml`

```
dependencies:
- otp-test
```

## Development

[![Build Status](https://travis-ci.org//Haskell-otp-test.svg?branch=master)](https://travis-ci.org//Haskell-otp-test)
[![Github](https://img.shields.io/github/commits-since//haskell-otp-test/v0.0.0.0.svg)](https://img.shields.io/github/commits-since//haskell-otp-test/v0.0.0.0.svg)
[![Hackage Dependencies](https://img.shields.io/hackage-deps/v/otp-test.svg)](http://packdeps.haskellers.com/feed?needle=otp-test)

Follow the [developer guidelines](https://romanandreg.gitbooks.io/otp-test/content/developer-guidelines.html)