{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE NoImplicitPrelude #-}
module Types where

import RIO
import Data.UUID (UUID)
import Data.Binary (Binary)
import Data.Sequence (Seq)

import System.Random (StdGen, mkStdGen)

import Control.Distributed.Process.Internal.Types (Process(..))

data Tuple a b
  = Tuple !a !b
  deriving (Generic, Typeable)

instance (Binary a, Binary b) => Binary (Tuple a b)

data Message =
  Message
  {
    msgId        :: !UUID
  , msgValue     :: !Double
  }
  deriving (Generic, Typeable, Show)

instance Binary Message

data SystemState =
  SystemState
  {
    stRandomGen :: !StdGen
  -- , stAccList   :: !(Seq Message)
  , stAccLength   :: !Integer
  }
  deriving (Generic, Typeable, Show)

-- Let's use async API on it's fullest
instance MonadUnliftIO Process where
  askUnliftIO = Process $ ReaderT $ \r ->
     withUnliftIO $ \u ->
       return (UnliftIO (unliftIO u . flip runReaderT r . unProcess))


newSystemState :: Int -> IO (IORef SystemState)
newSystemState withSeed =
  newIORef (SystemState (mkStdGen withSeed)
                        -- mempty)
                        0)
