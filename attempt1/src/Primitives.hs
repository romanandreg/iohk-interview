{-# LANGUAGE NoImplicitPrelude #-}
module Primitives where

import RIO
import qualified Control.Distributed.Process as DP
import qualified Control.Distributed.Process.Node as DPN
import qualified Control.Distributed.Backend.P2P as P2P
import Network.Socket (HostName, ServiceName)
import Network.Transport (closeTransport)
import Network.Transport.TCP (createTransport, defaultTCPParameters)

-- | Creates tcp local node which used by 'bootstrap'
createLocalNode
  :: HostName
  -> ServiceName
  -> (ServiceName -> (HostName, ServiceName))
  -> DP.RemoteTable
  -> IO (DPN.LocalNode, IO ())
createLocalNode host port mkExternal rTable = do
    transport <- either (error . show) id
                 <$> createTransport host port mkExternal defaultTCPParameters
    localNode <- DPN.newLocalNode transport rTable
    return (localNode, closeTransport transport)


-- NOTE: silly library doesn't close the Network :flip: -- RG

-- | Like 'bootstrap' but use 'forkProcess' instead of 'runProcess'. Returns local node and pid of given process
bootstrapNonBlocking
  :: HostName
  -> ServiceName
  -> (ServiceName -> (HostName, ServiceName))
  -> DP.RemoteTable
  -> [DP.NodeId]
  -> DP.Process ()
  -> IO (DPN.LocalNode, DP.ProcessId, IO ())
bootstrapNonBlocking host port ext rTable seeds prc = do
    (node, cTransport) <- createLocalNode host port ext rTable
    _ <- DPN.forkProcess node $ P2P.peerController seeds
    pid <- DPN.forkProcess node $ P2P.waitController prc
    return (node, pid, cTransport)

-- waitEither :: DP.Process a -> DP.Process b -> DP.Process (Either a b)
-- waitEither = do
