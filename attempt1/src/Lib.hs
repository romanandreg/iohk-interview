{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE NoImplicitPrelude #-}
module Lib where

import RIO

import qualified Data.UUID.V4 as UUID
-- import qualified Data.Sequence as Seq

import qualified Control.Distributed.Backend.P2P as P2P
import qualified Control.Distributed.Process as DP

import System.Random (randomR)

import Util
import Types


delaySeconds :: Int -> DP.Process ()
delaySeconds = threadDelay . (*1000100)

runDistributedSystem :: Int -> Int -> Int -> DP.Process ()
runDistributedSystem withSeed sendForSeconds waitForSeconds = do
  stRef <- liftIO $ newSystemState withSeed
  receiver <- DP.spawnLocal (messageReceiver stRef)
  sender   <- DP.spawnLocal (messageSender stRef)
  delaySeconds sendForSeconds
  DP.kill sender "sendFor period ended"
  delaySeconds (waitForSeconds - 1)
  DP.kill receiver "waitFor period ended"

  -- result <- stAccList <$> readIORef stRef
  -- logErr $ tshow (length result, foldr (\(i, v) acc -> (i * msgValue v) + acc) 0 $ Seq.zip (Seq.fromList [1..]) result)

  result <- stAccLength <$> readIORef stRef
  logErr $ tshow result

createMessage :: IORef SystemState -> IO Message
createMessage stRef = do
    nextId <- UUID.nextRandom
    buildMessage nextId
  where
    buildMessage nextId =
      atomicModifyIORef
        stRef
        -- (\SystemState {stRandomGen, stAccList } ->
        --    let (randVal, randGen1) = randomR (0, 1) stRandomGen
        --        msg = Message nextId randVal

        --    in ( SystemState { stRandomGen = randGen1
        --                     , stAccList = stAccList Seq.|> msg
        --                     }
        --       , msg
        --       )
        -- )
        (\SystemState {stRandomGen, stAccLength } ->
           let (randVal, randGen1) = randomR (0, 1) stRandomGen
               msg = Message nextId randVal

           in ( SystemState { stRandomGen = randGen1
                            , stAccLength = stAccLength + 1
                            }
              , msg
              )
        )

messageSender :: IORef SystemState -> DP.Process ()
messageSender stRef = forever $ do
  msg <- liftIO $ createMessage stRef
  threadDelay 1001000
  P2P.getPeers >>= \peers -> DP.say ("==== PEERS =" <> show peers)
  DP.say "===== sending msg"
  P2P.nsendPeers "otp-test" msg

messageReceiver :: IORef SystemState -> DP.Process ()
messageReceiver stRef = do
    me <- DP.getSelfPid
    DP.register "otp-test" me
    forever $ DP.receiveWait [ DP.match handleNewMessage ]
  where
    handleNewMessage :: Message -> DP.Process ()
    handleNewMessage _msg = do
      DP.say "+==== receiving msg"
      atomicModifyIORef
        stRef
        -- (\SystemState {stAccList, ..} ->
        --    (SystemState {stAccList = stAccList Seq.|> msg, .. }, ())
        -- )
        (\SystemState {stAccLength, ..} ->
           (SystemState {stAccLength = stAccLength + 1, .. }, ())
        )
