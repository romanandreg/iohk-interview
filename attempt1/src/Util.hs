{-# LANGUAGE NoImplicitPrelude #-}
module Util where

import RIO
import Data.Text.IO (hPutStrLn)

-- Logging mechanisms in RIO are a mess right now
logErr :: MonadIO m => Text -> m ()
logErr = liftIO . hPutStrLn stderr
