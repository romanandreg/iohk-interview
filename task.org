* CH/OTP Task
  :PROPERTIES:
  :ID:       03b04753-510c-4c19-97cc-7b4a3c52515c
  :CREATED:  <2018-03-30 Fri 18:48>
  :END:

** Observations
- "Your code will run under different network failure scenarios"; some I can I think of
  + discovered node fails
    * extra:
      - we can implement an algorithm that uses other nodes as proxies that
        forward a request, if a response is met, then things are working fine

  + no response from a node
    * we need to have a timeout to detect this scenario, and apply same logic as
      discovered node fails

  + slow ack responses
    * same as no response, a timeout is used

  + corrupted data is received
    * we forgo the message and emit a warning message

** Implementation Notes

*** Quick overview

I need to create a system that:

- listens to messages from other nodes
  + store to node state
  + ack the message

- sends messages to other nodes
  + expect ack for each of them

*** What should the message type contain?

  - Unique UUID
  - Random number
  - Tuple known by this node so far
  - Timestamp

*** Transport considerations

  + There is a distributed-process-p2p that looks like a good candidate for a
    quick run, dpwiz is the author, and he delivers good libs at times

  + Write down a document with considerations if you would do a
    distributed-process-p2p replacement on your own

** Investigation

The distributed-process-p2p is short enough and it was achievable to understand
its code

** PAUSED Implementation
   :PROPERTIES:
   :ID:       93e08676-a95b-47c3-9631-6d56c551f26c
   :CREATED:  <2018-03-30 Fri 21:19>
   :Effort:   4:00
   :END:
   :LOGBOOK:
   - State "PAUSED"     from "STARTED"    [2018-03-30 Fri 21:54]
   - State "STARTED"    from              [2018-03-30 Fri 21:25]
   :END:
   :CLOCK:
   CLOCK: [2018-03-30 Fri 20:50]--[2018-03-30 Fri 21:54] =>  1:04
   :END:

- How can I do a race with ~Process~ monad
  + Found ~distributed-process-async~, sadly is not up to date, so is a no go
  + Lets implement ~race~ in process? Better not, too many yak shaves
  + Make ~Process~ implement ~MonadUnliftIO~ so that I can use Async API?
    + Concerned around ~Async~ threads and its cancellation policies, it will
      likely mess with Distributed Process state, implement and test

- Try to implement an algorithm without race

- After modeling a high level algorithm, I'm coming to the realization that the
  ~i~ value from the formula needs to be made in concensus, making the exercise
  trickier than I originally thought... time for a break

- I managed to use effectively the p2p endpoint, the output is rather verbose, so I will
  copy/paste the code here, and work towards improving upon it

** Questions

  - "The larger your score with a given sequence of random number is, the better".

  What score are we talking about here, the length of the state list per node?

  - What does (i . m_i) means? the product of the two?

  - Does the second value of the node tuple need to be the same in the output of
    all nodes?
