# iohk-interview

> Hard fun is, of course, the idea that we take pleasure in accomplishing
> something difficult: the joy in meeting and mastering a challenge. -- David
> Williamson Shaffer

## Table Of Contents

* [TL;DR](#setup)
* [Summary of solution](#summary)
* [Summary of time spent](#summary)
* [Project Structure](#project-structure)
* [Next improvements](#improvements)

## TL;DR - a.k.a How to setup

The project requires a configuration to be located at a relative path of
the binary execution called `resources/config.yml`. You may also put a
configuration file in `/usr/local/iohk-interview/config.yml` that will take
precedence over the values of the first file mentioned.

In this configuration file, there must be an entry called `other-hosts` that
contains a list of all the hosts. To understand what are all the possible
options, please check the `attempt2/resources/config.spec.yml`

### Run the binary

```bash
cd ./attempt2
stack build --copy-bins --local-bin-path ./bin/
IOHK_PORT=8080 IOHK_HOST="10.0.0.1" ./bin/iohk-interview --send-for 100 --wait-for 3 --with-seed 50
```

## Summary of Solution

### Algorithm used

For this challenge, I decided to use the Raft algorithm as the way to manage
consensus between various nodes. Why? It's [protocol
documentation](https://raft.github.io/) was very approachable, it also looked
feasible to implement in a matter of days.

### State representation

Having experience modeling state machines in highly concurrent environments, the
approach that I've found best is event-sourcing, for this reason, I decided to
model the system using an approach similar to Elm's. Each component provides an
`initState`, `update` and `subscriptions` function, and we compose them on higher
level `update` functions.

The other benefit is that all the state modification functions are pure,
allowing us to replace the monad used in the side-effects if we eventually
decide Cloud Haskell is not the best platform for our system.

There are currently three components:

- `System`
    - `P2P`  - Reponsible of node discovery
    - `Raft` - Responsible of the raft protocol

#### Raft state reducers

The Raft protocol has three different states, and each of them is represented in a
module in the `Raft/Core` folder. Each State handles _all_ the various
messages, even though they would not care for them. Given this there is some
duplicated code, this was done on purpose so that I can easily modify the
behavior of one state without affecting the others. I made this decision on my
third day of development after a re-implementation of the model.

### Testing

I was able to test the system in Google Cloud Engine, with at least eight different
nodes. I noticed my solution produced starvation on some of the nodes when
running many at a time. To work around this problem, I tweaked the heartbeat and
election delay of the Raft protocol, sometimes increasing them, and it improved
the performance of the system as a whole. Given these delays are so significant, I added
some CLI options to specify them.

## Project Structure

* `attempt1` - This was a small project I created to test the functionality of
  distributed-process-p2p library, as well as building a binary that I could use
  for testing; this was a write-off from the getgo just to re-familiarize with
  the distributed-process library, which I have stopped using four years ago or so.

* `attempt2` - This folder contains the project you should be testing, I
  generated the stack project from a personal template.

* `attempt2/app` - Contains a file that parses the configuration of my program
  using `etc` and then calls the main program with those values.

* `attempt2/src/Core.hs` - This file is a bit convoluted, is responsible of

    - Setup of the Cloud Haskell Node
    - Merging the P2P and Raft sub-systems
    - Starting a thread that requests state updates
    - Creates a state machine using an Elm style architecture
    - Sets up the interview test, runs for `--send-for` amount of time and then
      halts just after finishing the `--wait-for` time

* `attempt2/src/P2P.hs` - Contains all the node discovery logic, highly inspired
   by `distributed-process-p2p`, it builds a state machine using event sourcing;
   the state from this event sourcing is later used in the Raft implementation.

* `attempt2/src/Raft/Core.hs` - Contains the state machine logic of the Raft
   protocol, it is sub-divided in three more files:

   - `attempt2/src/Raft/Core/Leader.hs`
   - `attempt2/src/Raft/Core/Candidate.hs`
   - `attempt2/src/Raft/Core/Follower.hs`

   each of those files contains logic for the event reducer when the node is running
   on those modes.

## Summary of time spent

1. First day (Friday - March 30th) I was spent getting re-familiarized with Cloud Haskell and
   investigating multiple consensus algorithms, SWIM and Raft were seriously
   considered.

2. Second day (Sunday) I built the crux of the algorithm.

3. Third day (Monday), I re-implemented some parts of the algorithm, given the
   initial model representation I envisioned was lacking.

4. Fourth day (Tuesday night), I did improvements of the tracing of
   of the system, which helped catch bugs quickly

5. Fifth day (Wednesday and Thursday night), I needed to change the algorithm
   because it was discarding messages from nodes. I figured from the document
   that all interactions mattered, I had to do a considerate refactoring to
   accomplish this (a.k.a I <3 Haskell).

6. Sixth day (Friday) I spent the day setting up a test environment in different
   Cloud providers, and also figuring out why my nodes did not connect. I used
   the `DISTRIBUTED_PROCESS_xxx` flags [documented
   here](https://github.com/haskell-distributed/distributed-process/blob/master/src/Control/Distributed/Process/Debug.hs#L67)
   to figure out that the bind port to `0.0.0.0` was not indicative of accepting
   request from any node, I needed to know the public IP in advanced to run the
   algorithm effectively.

7. Seventh-day (Saturday - April 7th) I wrote this README.

## Improvements

Some immediate improvements:

* Allow defining TPC parameters on the node: I would extend the configuration
  File to allow entries given on the `Network.Transport.defaultTCPParameters`
  record

* Split out `Core.hs` into more than one module

* Improve logging infrastructure: `Control.Distributed.Process.say` is a very poor
  mechanism for logging

* Do some integration tests using
  [toxiproxy](https://github.com/jpittis/toxiproxy-haskell) in the middle

* Perform an investigation of how to keep many nodes running on a small
  heartbeat delay

## Conclusion

This challenge was harder than expected, but it was a lot of fun, thank you for
the opportunity. Looking forward to hearing a review from you.

Cheers.
