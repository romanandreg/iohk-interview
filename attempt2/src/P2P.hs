{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE MonoLocalBinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NoImplicitPrelude #-}
module P2P
  (
    P2PState (..)
  , Msg (..)
  , initP2PListener
  , update
  , initState
  , renderMsg
  , renderState
  ) where

import RIO
import Data.Typeable (typeOf)
import qualified RIO.Set as Set
import Data.Binary (Binary)

import Control.Distributed.Process (NodeId, Process, ProcessId, MonitorRef)
import qualified Control.Distributed.Process as DP

--------------------------------------------------------------------------------

raftP2PService :: String
raftP2PService = "Raft::P2P"

--------------------------------------------------------------------------------

sendPeerCheckIn :: TQueue (Process ()) -> ProcessId -> Set ProcessId -> Process Msg
sendPeerCheckIn cloudListenerQueue peerPid selfKnownPeers = do
  (sp, rp) <- DP.newChan

  atomically $ writeTQueue cloudListenerQueue $ do
    pid <- DP.getSelfPid
    DP.send peerPid (PeerCheckIn pid sp)

  peerKnownPeers <- DP.receiveChan rp

  let unknownPeers =
        Set.difference peerKnownPeers selfKnownPeers

  forM_ unknownPeers $ \aPeerPid ->
    atomically $ writeTQueue cloudListenerQueue $ do
      let nodeId = DP.processNodeId peerPid
      _monitorRef <- DP.monitor aPeerPid
      DP.whereisRemoteAsync nodeId raftP2PService

  return $ PeerCheckedIn unknownPeers

replyCheckIn :: Set ProcessId -> DP.SendPort (Set ProcessId) -> Process Msg
replyCheckIn knownPeers knownPeersPort = do
  DP.sendChan knownPeersPort knownPeers
  return PeerCheckInReplied

unregisterPeer :: TQueue (Process ()) -> ProcessId -> MonitorRef -> Process Msg
unregisterPeer cloudListenerQueue peerId monitor = do
  atomically $ writeTQueue cloudListenerQueue $ DP.unmonitor monitor
  return $ PeerUnregistered peerId

sendPeerQuery :: Set ProcessId -> DP.SendPort (Set ProcessId) -> Process Msg
sendPeerQuery knownPeers knownPeersPort = do
  DP.sendChan knownPeersPort knownPeers
  return PeerQueryReplied

--------------------------------------------------------------------------------

renderState :: P2PState -> String
renderState P2PState {p2pActiveNodes} =
  "[peerCount: " <> show (length p2pActiveNodes) <> "]"

renderMsg :: Msg -> Maybe String
renderMsg Noop = Nothing
renderMsg msg =
  Just . ("P2P|" <>) $ case msg of
    Noop -> ""
    P2PInitialized {} -> "P2PInitialized"
    PeerDiscovered {} -> "PeerDiscovered"
    PeerCheckIn {} -> "PeerCheckIn"
    PeerCheckedIn {} -> "PeerCheckedIn"
    PeerCheckInReplied -> "PeerCheckedInReplied"
    PeerFailed _ monitor -> "PeerFailed|" <> show monitor
    PeerUnregistered {} -> "PeerUnregistered"
    PeerQuery {} -> "PeerQuery"
    PeerQueryReplied {} -> "PeerQueryReplied"

--------------------------------------------------------------------------------

data P2PState
  = P2PState
    {
      p2pNodeId            :: !NodeId
    , p2pAllPeers          :: !(Set NodeId)
    , p2pActivePeers       :: !(Set ProcessId)
    , p2pActiveNodes       :: !(Set NodeId)
    , cloudListenerQueue   :: !(TQueue (Process ()))
    }
  deriving (Show)

-- Moving fast and breaking things...
instance Typeable a => Show (TQueue a) where
  show = show . typeOf

data Msg
  = Noop
  | P2PInitialized !ProcessId
  | PeerDiscovered !ProcessId
  | PeerCheckIn !ProcessId !(DP.SendPort (Set ProcessId))
  | PeerCheckedIn !(Set ProcessId)
  | PeerCheckInReplied
  | PeerFailed !ProcessId !MonitorRef
  | PeerUnregistered !ProcessId
  | PeerQuery !(DP.SendPort (Set ProcessId))
  | PeerQueryReplied
  deriving (Generic, Typeable, Show)

instance Binary Msg

--------------------------------------------------------------------------------

initP2PListener :: P2PState -> Process (STM Msg, Process ())
initP2PListener p2p = do
    queue <- newTQueueIO
    processId <- DP.spawnLocal $ processListener queue

    DP.link processId
    return ( readTQueue queue
           , DP.kill processId "shutdown"
           )
  where
    isPeerDiscovery :: DP.WhereIsReply -> Bool
    isPeerDiscovery (DP.WhereIsReply service mpid) =
      service == raftP2PService && isJust mpid

    onDiscovery :: TQueue Msg -> DP.WhereIsReply -> Process ()
    onDiscovery queue discovery =
      case discovery of
        DP.WhereIsReply _ (Just pid) ->
          atomically (writeTQueue queue $ PeerDiscovered pid)
        _ ->
          return ()

    onMonitor :: TQueue Msg -> DP.ProcessMonitorNotification -> Process ()
    onMonitor queue (DP.ProcessMonitorNotification ref pid _reason) = do
      atomically (writeTQueue queue (PeerFailed pid ref))

    onP2PMsg :: TQueue Msg -> Msg -> Process ()
    onP2PMsg queue msg =
      atomically (writeTQueue queue msg)

    processListener :: TQueue Msg -> Process ()
    processListener queue = do
      pid <- DP.getSelfPid
      DP.register raftP2PService pid
      atomically $ writeTQueue queue (P2PInitialized pid)

      mapM_ (flip DP.whereisRemoteAsync raftP2PService) (p2pAllPeers p2p)

      forever $ DP.receiveWait [
          DP.matchIf isPeerDiscovery (onDiscovery queue)
        , DP.match (onMonitor queue)
        , DP.match (onP2PMsg queue)
        , DP.matchSTM (readTQueue $ cloudListenerQueue p2p) id
        ]

--------------------------------------------------------------------------------

initState :: NodeId -> [NodeId] -> Process P2PState
initState nodeId seeds = do
  cloudListenerQueue <- newTQueueIO
  return
    $ P2PState
      {
        p2pNodeId = nodeId
      , p2pAllPeers = Set.fromList seeds
      , p2pActivePeers = Set.empty
      , p2pActiveNodes = Set.empty
      , cloudListenerQueue
      }

update :: P2PState -> Msg -> (P2PState, [Process Msg])
update st@P2PState { p2pNodeId, p2pAllPeers, p2pActivePeers, p2pActiveNodes, cloudListenerQueue } msg =
  case msg of
    Noop ->
      ( st, [] )

    P2PInitialized myId ->
      ( st { p2pActivePeers = Set.insert myId p2pActivePeers
           , p2pActiveNodes = Set.insert (DP.processNodeId myId) p2pActiveNodes
           }
      , []
      )

    PeerDiscovered peerPid ->
      ( st { p2pActivePeers = Set.insert peerPid p2pActivePeers
           , p2pActiveNodes = Set.insert (DP.processNodeId peerPid) p2pActiveNodes }
      , [ sendPeerCheckIn cloudListenerQueue peerPid p2pActivePeers ]
      )

    PeerCheckIn peerId knownPeerPort ->
      ( P2PState { p2pActivePeers = Set.insert peerId p2pActivePeers
                 , p2pActiveNodes = Set.insert (DP.processNodeId peerId) p2pActiveNodes
                 , .. }
      , [ replyCheckIn p2pActivePeers knownPeerPort ] )

    PeerCheckedIn unknownPeers ->
      ( P2PState { p2pActivePeers = Set.union p2pActivePeers unknownPeers
                 , p2pActiveNodes = Set.union p2pActiveNodes (Set.map DP.processNodeId unknownPeers)
                 , .. }
      , []
      )

    PeerCheckInReplied ->
      ( st, [] )

    PeerFailed peerId monitor ->
      ( P2PState { p2pActivePeers = Set.delete peerId p2pActivePeers
                 , p2pActiveNodes = Set.delete (DP.processNodeId peerId) p2pActiveNodes
                 , .. }
      , [ unregisterPeer cloudListenerQueue peerId monitor ]
      )

    PeerUnregistered _peerId ->
      ( st, [] )

    PeerQuery replyPort ->
      ( st
      , [ sendPeerQuery p2pActivePeers replyPort ]
      )

    PeerQueryReplied ->
      ( st, [] )
