{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE NoImplicitPrelude #-}
module Core where

import qualified Prelude
import RIO

import GHC.Conc (orElse)
import Control.Distributed.Process (Process, NodeId)
import qualified Control.Distributed.Process as DP
import qualified Control.Distributed.Process.Node as DPN
import qualified Network.Transport as Net
import qualified Network.Socket as Net
import Data.Binary (Binary)

-- import Text.Show.Pretty (ppShow)

import qualified System.Random as Random

import Util
import qualified P2P
import qualified Raft.Types as Raft
import qualified Raft.Core as Raft
import Primitives (createLocalNode)

data SystemState input output
  = SystemState
  {
    p2p  :: !P2P.P2PState
  , raft :: !(Raft.RaftState input output)
  }

data Msg input output
  = RaftMsg (Raft.Msg input output)
  | P2PMsg  P2P.Msg
  | Noop
  deriving (Generic, Typeable, Show)

--------------------------------------------------------------------------------

renderState :: SystemState input output -> String
renderState SystemState {raft, p2p} =
  P2P.renderState p2p <> Raft.renderState raft

renderMsg :: Msg input output -> Maybe String
renderMsg topMsg =
  case topMsg of
    Noop         -> Nothing
    RaftMsg msg  -> Raft.renderMsg msg
    P2PMsg msg   -> P2P.renderMsg msg

logMsg :: SystemState input output -> Msg input output -> Process (Msg input output)
logMsg st msg = do
  case renderMsg msg of
    Nothing -> return ()
    Just msgStr -> DP.say $ renderState st <> " - " <> msgStr
  return Noop

update
  :: ( Typeable input, Binary input, Show input
     , Typeable output, Binary output, Show output
     )
  => NodeId
  -> SystemState input output
  -> Msg input output
  -> (SystemState input output, [Process (Msg input output)])
update myNodeId st@SystemState {p2p, raft} sysMsg =
  case sysMsg of
    Noop ->
      (st, [])

    RaftMsg msg ->
      let
        (raft', replies) =
          Raft.update myNodeId (P2P.p2pActiveNodes p2p) raft msg

        st' = SystemState { raft = raft', .. }
      in
        ( st'
        , (RaftMsg <$$> replies) ++ [ logMsg st' sysMsg ]
        )

    P2PMsg msg ->
      let
        (p2p', replies) =
          P2P.update p2p msg
        st' = SystemState { p2p = p2p', .. }
      in
        ( st'
        , (P2PMsg <$$> replies) ++ [ logMsg st' sysMsg ]
        )

initState
  :: Raft.RaftDelays
  -> output
  -> ((Integer, output) -> input -> output)
  -> Random.StdGen
  -> NodeId
  -> [NodeId]
  -> Process (SystemState input output)
initState raftDelays raftInitialValue raftModifyState raftRandomGen nodeId seeds = do
  p2p <- P2P.initState nodeId seeds
  raft <-
    liftIO $ Raft.initState $ Raft.RaftArgs { raftInitialValue
                                            , raftRandomGen
                                            , raftModifyState
                                            , raftNode = nodeId
                                            , raftDelays
                                            }

  return $ SystemState { p2p, raft }

subscriptions
  :: ( Typeable input, Binary input,  Show input
     , Typeable output, Binary output,  Show output
     )
  => SystemState input output
  -> Process (STM (Msg input output), DP.Process ())
subscriptions SystemState {p2p, raft}= do
  (getP2PMsg, stopP2PListener) <- P2P.initP2PListener p2p
  (getRaftMsg, stopRaftListener) <- Raft.subscriptions raft

  let
    getMsg =
      (P2PMsg <$> getP2PMsg)
      `orElse` (RaftMsg <$> getRaftMsg)

  return (getMsg, stopP2PListener >> stopRaftListener)

--------------------------------------------------------------------------------

pauseSystem :: SystemState input output -> Process ()
pauseSystem SystemState {raft} =
  Raft.pauseSystem raft

startProgram
  :: ( Binary input, Typeable input,  Show input
     , Binary output, Typeable output,  Show output
     )
  => SystemState input output
  -> Process ( input -> IO ()
             , Process output
             , Process (Maybe Integer)
             , Process ()
             , Process ()
             )
startProgram st0 = do
  myNodeId <- DP.processNodeId <$> DP.getSelfPid

  stateMachineQueue <- newTQueueIO
  programInputQueue <- newTQueueIO

  (getSystemMsg, cleanup) <- subscriptions st0

  let getMsg =
        getSystemMsg
        `orElse` readTQueue stateMachineQueue
        `orElse` readTQueue programInputQueue

  let
    stateMachineLoop st = do
      msg <- atomically getMsg
      let (st1, replies) = update myNodeId st msg
      msgList <- sequence replies
      mapM_ (atomically . writeTQueue stateMachineQueue) msgList
      stateMachineLoop st1

    proposeNewValue st reqPayload = do
      let SystemState {raft} = st
      void $ Raft.requestChange raft reqPayload

    getCurrentValue = do
      (sp, rp) <- DP.newChan
      atomically
        $ writeTQueue programInputQueue
        $ RaftMsg
        $ Raft.requestCurrentState sp
      DP.receiveChan rp

    getCurrentMsgCount = do
      (sp, rp) <- DP.newChan
      atomically
        $ writeTQueue programInputQueue
        $ RaftMsg
        $ Raft.requestMsgCount sp
      DP.receiveChanTimeout 100 rp

  loopProcess <- DP.spawnLocal $ stateMachineLoop st0
  DP.link loopProcess

  return ( proposeNewValue st0
         , getCurrentValue
         , getCurrentMsgCount
         , pauseSystem st0
         , cleanup >> DP.kill loopProcess "shutdown"
         )

--------------------------------------------------------------------------------
-- What Main will have

startCloud :: Net.HostName -> Net.ServiceName -> DP.RemoteTable -> Process () -> IO ()
startCloud host port remoteTable action = do
  bracket (createLocalNode host port (const (host, port)) remoteTable)
          (\(localNode, transport) -> do
             DPN.closeLocalNode localNode
             liftIO $ Net.closeTransport transport)
          (\(localNode, _) ->
             DPN.runProcess localNode action)


-- runInterviewApp
--   :: MVar (Integer, Double)
--   -> Int
--   -> Int
--   -> Int
--   -> [NodeId]
--   -> Process ()
runInterviewApp :: MVar (Integer, Double) -> Raft.RaftDelays -> Int -> Int -> Int -> [NodeId] -> Process ()
runInterviewApp resultVar raftDelays withSeed sendForSeconds waitForSeconds hosts = do
  myNodeId <- DP.getSelfPid
  let
    nodeId = DP.processNodeId myNodeId
    stdgenA = Random.mkStdGen withSeed
    (firstRand, stdgenB) = Random.randomR (0.0, 1.0) stdgenA
    firstValue = (1, firstRand)

    updateState :: (Integer, (Integer, Double)) -> Double -> (Integer, Double)
    updateState (currentLength, (_, st)) randVal =
      (currentLength + 1, st + (randVal * (fromIntegral currentLength + 1)))

  st0 <- initState raftDelays firstValue updateState stdgenA nodeId hosts

  DP.bracket (startProgram st0)
          (\(_, _, _, _, stop) -> stop)
          (\(proposeNewValue, getCurrentValue, _, pause, _) -> do
             let
               requesterLoop stdgen = do
                 let (randVal :: Double, stdgen1) = Random.randomR (0.0, 1.0) stdgen
                 liftIO $ proposeNewValue randVal
                 requesterLoop stdgen1

             senderPid <- DP.spawnLocal (requesterLoop stdgenB
                                         `DP.finally` DP.say "==== NOTICE: SENDER IS KAPUT ====")

             threadDelay (sendForSeconds * 1000100)
             pause
             DP.say "==== NOTICE: SEND FOR DONE ===="
             DP.kill senderPid "send period ended"

             threadDelay ((waitForSeconds * 1000100) - 500)
             getCurrentValue >>= putMVar resultVar
             DP.say "==== NOTICE: WAIT FOR SECONDS DONE ===="
        )


main
  :: Net.HostName
  -> Net.ServiceName
  -> DP.RemoteTable
  -> [DP.NodeId]
  -> Raft.RaftDelays -> Int -> Int -> Int -> IO ()
main host port remoteTable hosts raftDelays withSeed sendForSeconds waitForSeconds  = do
  resultVar <- newEmptyMVar
  finally (startCloud host port remoteTable $
           runInterviewApp resultVar raftDelays withSeed sendForSeconds waitForSeconds hosts)
          (takeMVar resultVar >>= Prelude.print)

-- main "127.0.0.1" "5555" DPN.initRemoteTable [DP.NodeId $ Net.EndPointAddress "127.0.0.1:8090:0"] 1 10 5
