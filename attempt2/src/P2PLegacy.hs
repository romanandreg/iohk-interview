{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE MonoLocalBinds #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NoImplicitPrelude #-}
module P2PLegacy (
    -- * Starting peer controller
    bootstrap,
    bootstrapNonBlocking,
    peerController,
    -- * Nodes manipulation
    makeNodeId,
    getPeers,
    getCapable,
    nsendPeers,
    nsendCapable,
    -- * Auxiliary
    createLocalNode,
    waitController
) where

import RIO

import Control.Distributed.Process (Process, ProcessId, SendPort, NodeId(..), ProcessMonitorNotification(..), WhereIsReply(..), MonitorRef, RemoteTable)
import Control.Distributed.Process.Node (LocalNode)
import qualified Control.Distributed.Process                as DP
import qualified Control.Distributed.Process.Node           as DPN
import Control.Distributed.Process.Serializable (Serializable)
import Network.Transport (EndPointAddress(EndPointAddress))
import Network.Socket (HostName, ServiceName)
import Network.Transport (Transport)
import Network.Transport.TCP (TCPParameters, createTransport)

import qualified RIO.ByteString as BS
import qualified RIO.Set as S
import Data.Maybe (isJust)

-- * Peer-to-peer API

type Peers = S.Set ProcessId

data PeerState = PeerState { p2pPeers :: MVar Peers }

initPeerState :: Process PeerState
initPeerState = do
  self <- DP.getSelfPid
  peers <- DP.liftIO $ newMVar (S.singleton self)
  return $! PeerState peers

-- ** Initialization

-- | Make a NodeId from "host:port" string.
makeNodeId :: ByteString -> NodeId
makeNodeId addr =
  NodeId . EndPointAddress . BS.concat $ [addr, ":0"]

-- | Start a controller service process and aquire connections to a swarm.
bootstrap
  :: HostName
  -> ServiceName
  -> TCPParameters
  -> (ServiceName -> (HostName, ServiceName))
  -> RemoteTable
  -> [NodeId]
  -> Process ()
  -> IO ()
bootstrap host port tcpParams ext rTable seeds prc = do
  (node, _transport) <- createLocalNode host port tcpParams ext rTable
  _ <- DPN.forkProcess node $ peerController seeds
  DPN.runProcess node $ waitController prc

-- | Like 'bootstrap' but use 'forkProcess' instead of 'runProcess'. Returns local node and pid of given process
bootstrapNonBlocking
  :: HostName
  -> ServiceName
  -> TCPParameters
  -> (ServiceName -> (HostName, ServiceName))
  -> RemoteTable
  -> [NodeId]
  -> Process ()
  -> IO (LocalNode, Transport, ProcessId)
bootstrapNonBlocking host port tcpParams ext rTable seeds prc = do
  (node, transport) <- createLocalNode host port tcpParams ext rTable
  _ <- DPN.forkProcess node $ peerController seeds
  pid <- DPN.forkProcess node $ waitController prc
  return (node, transport, pid)

-- | Waits for controller to start, then runs given process
waitController :: Process a -> Process a
waitController prc = do
  res <- DP.whereis peerControllerService
  case res of
    Nothing -> threadDelay 100000 >> waitController prc
    Just _ -> do
      -- DP.say "Bootstrap complete."
      prc

-- | Creates tcp local node which used by 'bootstrap'
createLocalNode
  :: HostName
  -> ServiceName
  -> TCPParameters
  -> (ServiceName -> (HostName, ServiceName))
  -> RemoteTable
  -> IO (LocalNode, Transport)
createLocalNode host port tcpParams mkExternal rTable = do
    transport <- either (error . show) id
                 <$> createTransport host port mkExternal tcpParams
    node <- DPN.newLocalNode transport rTable
    return (node, transport)

peerControllerService :: String
peerControllerService = "Raft::P2P"

-- | A P2P controller service process.
peerController :: [NodeId] -> Process ()
peerController seeds = do
    state <- initPeerState
    DP.getSelfPid >>= DP.register peerControllerService
    mapM_ doDiscover seeds
    -- DP.say "P2P controller started."
    forever $ DP.receiveWait [ DP.matchIf isPeerDiscover $ onDiscover state
                             , DP.match $ onMonitor state
                             , DP.match $ onPeerRequest state
                             , DP.match $ onPeerQuery state
                             , DP.match $ onPeerCapable
                             ]

-- ** Discovery

doDiscover :: NodeId -> Process ()
doDiscover node = do
    -- DP.say $ "Examining node: " ++ show node
    DP.whereisRemoteAsync node peerControllerService

doRegister :: PeerState -> ProcessId -> Process ()
doRegister (PeerState{..}) pid = do
    pids <- DP.liftIO $ takeMVar p2pPeers
    if S.member pid pids then
      DP.liftIO $ putMVar p2pPeers pids
    else do
      -- DP.say $ "Registering peer:" ++ show pid
      _ <- DP.monitor pid

      DP.liftIO $ putMVar p2pPeers (S.insert pid pids)
      -- DP.say $ "New node: " ++ show pid
      doDiscover $ DP.processNodeId pid

doUnregister :: PeerState -> Maybe MonitorRef -> ProcessId -> Process ()
doUnregister PeerState{..} mref pid = do
    -- DP.say $ "Unregistering peer: " ++ show pid
    maybe (return ()) DP.unmonitor mref
    peers <- DP.liftIO $ takeMVar p2pPeers
    DP.liftIO $ putMVar p2pPeers (S.delete pid peers)

isPeerDiscover :: WhereIsReply -> Bool
isPeerDiscover (WhereIsReply service pid) =
    service == peerControllerService && isJust pid

onDiscover :: PeerState -> WhereIsReply -> Process ()
onDiscover _     (WhereIsReply _ Nothing) = return ()
onDiscover state (WhereIsReply _ (Just seedPid)) = do
    -- DP.say $ "Peer discovered: " ++ show seedPid
    (sp, rp) <- DP.newChan
    self <- DP.getSelfPid
    DP.send seedPid (self, sp :: SendPort Peers)
    -- DP.say $ "Waiting for peers..."
    peers <- DP.receiveChan rp

    known <- DP.liftIO $ readMVar (p2pPeers state)
    mapM_ (doRegister state) (S.toList $ S.difference peers known)

onPeerRequest :: PeerState -> (ProcessId, SendPort Peers) -> Process ()
onPeerRequest PeerState{..} (peer, replyTo) = do
    -- DP.say $ "Peer exchange with " ++ show peer
    peers <- DP.liftIO $ takeMVar p2pPeers
    if S.member peer peers
        then DP.liftIO $ putMVar p2pPeers peers
        else do
            _ <- DP.monitor peer
            DP.liftIO $ putMVar p2pPeers (S.insert peer peers)

    DP.sendChan replyTo peers

onPeerQuery :: PeerState -> SendPort Peers -> Process ()
onPeerQuery PeerState{..} replyTo = do
    -- DP.say $ "Local peer query."
    DP.liftIO (readMVar p2pPeers) >>= DP.sendChan replyTo

onPeerCapable :: (String, SendPort ProcessId) -> Process ()
onPeerCapable (service, replyTo) = do
    -- DP.say $ "Capability request: " ++ service
    res <- DP.whereis service
    case res of
        Nothing -> do
          -- DP.say "I can't."
          return ()
        Just pid -> do
          -- DP.say "I can!"
          DP.sendChan replyTo pid

onMonitor :: PeerState -> ProcessMonitorNotification -> Process ()
onMonitor state (ProcessMonitorNotification mref pid _reason) = do
    -- DP.say $ "Monitor event: " ++ show (pid, reason)
    doUnregister state (Just mref) pid

-- ** Discovery

-- | Get a list of currently available peer nodes.
-- Performs peer query
getPeers :: Process [NodeId]
getPeers = do
    -- DP.say $ "Requesting peer list from local controller..."
    (sp, rp) <- DP.newChan
    DP.nsend peerControllerService (sp :: SendPort Peers)
    DP.receiveChan rp >>= return . map DP.processNodeId . S.toList

-- | Poll a network for a list of specific service providers.
getCapable :: String -> Process [ProcessId]
getCapable service = do
    (sp, rp) <- DP.newChan
    nsendPeers peerControllerService (service, sp)
    -- DP.say "Waiting for capable nodes..."
    go rp []

    where
      go rp acc = do
        res <- DP.receiveChanTimeout 100000 rp
        case res of
          Just pid -> do
            -- DP.say "cap hit"
            go rp (pid:acc)
          Nothing -> do
            -- DP.say "cap done"
            return acc

-- ** Messaging

-- | Broadcast a message to a specific service on all peers.
nsendPeers :: Serializable a => String -> a -> Process ()
nsendPeers service msg =
  getPeers >>= mapM_ (\peer -> DP.nsendRemote peer service msg)

-- | Broadcast a message to a service of on nodes currently running it.
nsendCapable :: Serializable a => String -> a -> Process ()
nsendCapable service msg =
  getCapable service >>= mapM_ (\pid -> DP.send pid msg)
