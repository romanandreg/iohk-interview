{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE NoImplicitPrelude #-}
module Types where

import RIO
import Data.UUID (UUID)
import Data.Binary (Binary)
