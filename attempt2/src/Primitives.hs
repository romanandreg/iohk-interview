{-# LANGUAGE NoImplicitPrelude #-}
module Primitives where

import RIO
import qualified Control.Distributed.Process as DP
import qualified Control.Distributed.Process.Node as DPN
import Network.Socket (HostName, ServiceName)
import Network.Transport (Transport)
import Network.Transport.TCP (createTransport, defaultTCPParameters)

-- | Creates tcp local node which used by 'bootstrap'
createLocalNode
  :: HostName
  -> ServiceName
  -> (ServiceName -> (HostName, ServiceName))
  -> DP.RemoteTable
  -> IO (DPN.LocalNode, Transport)
createLocalNode host port mkExternal rTable = do
    transport <- either (error . show) id
                 <$> createTransport host port mkExternal defaultTCPParameters
    localNode <- DPN.newLocalNode transport rTable
    return (localNode, transport)
