{-# LANGUAGE NoImplicitPrelude #-}
module Util where

import RIO
import qualified RIO.Text as Text

import Data.Time.Clock (UTCTime, getCurrentTime)
import Data.Time.Clock.POSIX (posixSecondsToUTCTime, utcTimeToPOSIXSeconds)

import Control.Distributed.Process (NodeId(..))
import Network.Transport (EndPointAddress(EndPointAddress))

(<$$>) :: ( Functor f1, Functor f2) => (a -> b) -> f1 (f2 a) -> f1 (f2 b)
(<$$>) f = fmap (fmap f)
infixl 4 <$$>

hasMajority :: Int -> Int -> Bool
hasMajority currentCount totalCount =
  currentCount >= (totalCount `div` 2)

-- | Make a NodeId from "host:port" string.
makeNodeId :: String -> NodeId
makeNodeId addr =
  NodeId . EndPointAddress . Text.encodeUtf8 . Text.pack $ concat [addr, ":0"]

pickMedian :: [a] -> Maybe a
pickMedian xs =
  case drop (length xs `div` 2) xs of
    [] -> Nothing
    (a:_) -> Just a

timestampToUTC :: Integer -> UTCTime
timestampToUTC = posixSecondsToUTCTime . fromIntegral

utcToTimestamp :: UTCTime -> Integer
utcToTimestamp = round . utcTimeToPOSIXSeconds

getCurrentTimestamp :: MonadIO m => m Integer
getCurrentTimestamp = liftIO (utcToTimestamp <$> getCurrentTime)
