{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE NoImplicitPrelude #-}
module Raft.Util where

import RIO
import qualified RIO.Set as Set

import qualified Data.Sequence as Seq
import Data.Binary (Binary)

import Control.Distributed.Process (NodeId, Process)
import qualified Control.Distributed.Process as DP

import Raft.Types

import Util (hasMajority)

candidateToLeader :: RaftState input output -> RaftState input output
candidateToLeader RaftState {..} =
  RaftState { raftRole = Leader (LeaderState { requestQueue = Seq.empty, knownRequests = Set.empty }), ..}

updateTerm :: RaftTerm -> RaftState input output -> RaftState input output
updateTerm aRaftTerm RaftState {..} =
  RaftState { raftTerm = aRaftTerm, .. }

updateValue :: StateChangeOp input output -> RaftState input output -> RaftState input output
updateValue op st@RaftState {..} =
  case op of
    StateChangeReq {} ->
      st
    StateChangeResp {respPayload} ->
      RaftState { raftCurrentState = respPayload, .. }

updateMsgCount :: Integer -> RaftState input output -> RaftState input output
updateMsgCount newRaftMsgCount RaftState {..} =
  RaftState { raftMsgCount = newRaftMsgCount, .. }

startNewElection
  :: (Typeable input, Binary input, Typeable output, Binary output)
  => (NodeId -> RaftTerm -> DP.SendPort Bool -> RaftMsg input output)
  -> NodeId
  -> RaftTerm
  -> Set NodeId
  -> Process (RaftMsg input output)
startNewElection mkVote myNodeId raftTerm myPeers = do
    voteCount <- foldM requestVote initialVoteCount myPeers
    if hasMajority voteCount (length myPeers) then
      return $ BecameLeader raftTerm
    else
      return $ BecameFollower raftTerm Nothing

  where
    -- includes voting for myself
    initialVoteCount = 1

    requestVote :: Int -> NodeId -> Process Int
    requestVote acc peerNode  = do
      (sp, rp) <- DP.newChan
      DP.nsendRemote peerNode "Raft::Protocol" (mkVote myNodeId raftTerm sp)
      response <- DP.receiveChanTimeout 300100 rp -- 300 ms
      return $ case response of
        Just result ->
          if result then acc + 1 else acc
        Nothing ->
          acc

lockThread :: MVar () -> Process (RaftMsg input output)
lockThread aLock = do
  void $ tryTakeMVar aLock
  return RaftNoop

unlockThread :: MVar () -> Process (RaftMsg input output)
unlockThread aLock = do
  void $ tryPutMVar aLock ()
  return RaftNoop

getSelfNodeId :: Process NodeId
getSelfNodeId = DP.processNodeId <$> DP.getSelfPid

becomeFollower :: Monad m => RaftTerm -> NodeId -> m (RaftMsg input output)
becomeFollower theirTerm theirNodeId =
  return $ BecameFollower theirTerm (Just theirNodeId)

confirmProposal :: DP.SendPort () -> Process (RaftMsg input output)
confirmProposal sp = do
  DP.sendChan sp ()
  return RaftNoop
