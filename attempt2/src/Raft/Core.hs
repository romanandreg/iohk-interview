{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE NoImplicitPrelude #-}
module Raft.Core (RaftState, RaftDelays(..), RaftArgs(..), subscriptions, update, initState, renderState, renderMsg, pauseSystem, requestChange, requestCurrentState, requestMsgCount) where

import RIO
import GHC.Conc (orElse)
import qualified RIO.Set as Set
import Data.Binary (Binary)
import qualified Data.UUID.V4 as UUID
import qualified System.Random as Random

import Control.Distributed.Process (NodeId, Process)
import qualified Control.Distributed.Process as DP

import Raft.Types

import qualified Raft.Core.Leader as Leader
import qualified Raft.Core.Candidate as Candidate
import qualified Raft.Core.Follower as Follower
import Raft.Util (lockThread)
import qualified Util

--------------------------------------------------------------------------------

isSystemReady :: RaftState input output -> Bool
isSystemReady RaftState {raftRole} =
  case raftRole of
    Follower (FollowerState {raftLeader}) -> isJust raftLeader
    _ -> True

--------------------------------------------------------------------------------

update
  :: ( Typeable input, Binary input, Show input
     , Typeable output, Binary output, Show output )
  => NodeId
  -> Set NodeId
  -> RaftState input output
  -> Msg input output
  -> (RaftState input output, [Process (Msg input output)])
update myNodeId myPeers0 raftState msg =
  let
    myPeers = Set.filter (/= myNodeId) myPeers0

    RaftState { raftTerm } = raftState

  in case msg of
    Noop ->
      (raftState, [])

    RaftMsg (DraftHeartbeat ts) ->
      ( raftState, [return $ RaftMsg $ Heartbeat myNodeId raftTerm ts] )

    RaftMsg raftMsg ->
      let
        (raftState1, replies) = updateRaftMsg myNodeId myPeers raftState raftMsg
      in
        (raftState1, RaftMsg Util.<$$> replies)

    RequestCurrentState sp ->

      ( raftState
      , [ DP.sendChan sp (raftCurrentState raftState) *> return Noop ]
      )

    RequestMsgCount sp ->
      ( raftState
      -- only return msg count if system is ready; this way we can throtle
      -- the request sender
      , if isSystemReady raftState then
          [ DP.sendChan sp (raftMsgCount raftState) *> return Noop ]
        else
          []
      )

updateRaftMsg
  :: ( Typeable input, Binary input, Show input
     , Typeable output, Binary output, Show output
     )
  => NodeId
  -> Set NodeId
  -> RaftState input output
  -> RaftMsg input output
  -> (RaftState input output, [Process (RaftMsg input output)])
updateRaftMsg myNodeId myPeers raftState raftMsg =
  let
    RaftState {raftRole} = raftState

  in case raftRole of
    Follower followerState ->
      Follower.update myNodeId myPeers raftState followerState raftMsg
    Candidate ->
      Candidate.update myNodeId myPeers raftState raftMsg
    Leader leaderState ->
      Leader.update myNodeId myPeers raftState leaderState raftMsg

initState
  :: RaftArgs input output -> IO (RaftState input output)
initState RaftArgs {..} = do
  electionLock     <- newMVar ()
  netListenerLock  <- newMVar ()
  heartbeatLock    <- newEmptyMVar
  requestVar       <- newEmptyMVar
  return $ RaftState
    { raftCurrentState = raftInitialValue
    , raftRole =
      Follower (FollowerState { raftLeader = Nothing
                              , lastInteractionTimestamp = Nothing })
    , raftMsgCount = 0
    , raftTerm = 0
    , heartbeatCount = 0
    , ..
    }

pauseSystem :: RaftState input output -> Process ()
pauseSystem RaftState {netListenerLock, heartbeatLock, electionLock} = do
  void $ lockThread netListenerLock
  void $ lockThread heartbeatLock
  void $ lockThread electionLock

subscriptions
  :: ( Typeable input, Binary input, Show input
     , Typeable output, Binary output, Show output )
  => RaftState input output
  -> Process (STM (Msg input output), Process ())
subscriptions RaftState {raftRandomGen, raftDelays, netListenerLock, electionLock, heartbeatLock} = do
    netQueue       <- newTBQueueIO 100
    electionQueue  <- newTBQueueIO 5
    heartbeatQueue <- newTBQueueIO 1

    electionTimeoutPid  <- DP.spawnLocal (electionLoop raftRandomGen electionQueue)
    heartbeatTimeoutPid <- DP.spawnLocal (heartbeatLoop heartbeatQueue)
    raftListenerPid     <- DP.spawnLocal (netLoop netQueue)

    DP.link electionTimeoutPid
    DP.link heartbeatTimeoutPid
    DP.link raftListenerPid

    return ( RaftMsg <$>
             (readTBQueue netQueue
              `orElse` readTBQueue electionQueue
              `orElse` readTBQueue heartbeatQueue)
           , do DP.kill electionTimeoutPid "shutdown"
                DP.kill heartbeatTimeoutPid "shutdown"
                DP.kill raftListenerPid "shutdown"
           )
  where
    RaftDelays { electionDelayMicros, heartbeatDelayMicros } =
      raftDelays

    onRaftMsg netQueue msg =
      atomically $ writeTBQueue netQueue msg

    electionLoop gen netQueue = do
      let (electionDelay, gen1) =
            Random.randomR (electionDelayMicros, electionDelayMicros + 30000) gen

      readMVar electionLock
      threadDelay electionDelay

      ts <- Util.getCurrentTimestamp
      atomically $ writeTBQueue netQueue (CallElection ts)
      electionLoop gen1 netQueue

    heartbeatLoop netQueue = do
      forever $ do
        readMVar heartbeatLock
        threadDelay heartbeatDelayMicros
        ts <- Util.getCurrentTimestamp
        atomically $ writeTBQueue netQueue (DraftHeartbeat ts)

    netLoop netQueue = do
      DP.getSelfPid >>= DP.register "Raft::Protocol"
      forever $ do
        readMVar netListenerLock
        DP.receiveWait [ DP.match (onRaftMsg netQueue) ]

--------------------------------------------------------------------------------

renderRoleName :: Role input output -> String
renderRoleName role =
  case role of
    Follower  {} -> "-Follower "
    Candidate {} -> "*Candidate"
    Leader (LeaderState { requestQueue, knownRequests }) ->
      "+Leader ("
      <> show (length requestQueue)
      <> "/" <> show (length knownRequests)
      <> ")"

renderState :: RaftState input output -> String
renderState RaftState { raftRole, raftTerm } =
  "[" <> renderRoleName raftRole <> "][term:" <> show raftTerm <> "]"

renderRaftMsg :: RaftMsg input output -> Maybe String
renderRaftMsg RaftNoop = Nothing
renderRaftMsg Heartbeat {} = Nothing
renderRaftMsg DraftHeartbeat {} = Nothing
renderRaftMsg msg =
  Just . ("Raft|" <>) $ case msg of
    RaftNoop -> ""
    DraftHeartbeat {} -> ""
    RegisterLastInteraction{} -> "RegisterLastInteraction"
    CallElection {} -> "CallElection"
    Heartbeat {heartbeatSender, raftTerm} ->
      "Heartbeat|" <> show heartbeatSender <> "|" <> show raftTerm
    VoteRequested {raftCandidate, raftTerm} ->
      "VoteRequest|" <> show raftCandidate <> "|" <> show raftTerm
    VoteReplied {raftTerm} -> "VoteReplied|" <> show raftTerm
    StateChangeProposalRequested {requestSender,raftTerm} ->
      "StateChangeProposalRequested|" <> show requestSender <> "|" <> show raftTerm
    StateChangeProposalDrafted {raftLeader, raftTerm} ->
      "StateChangeProposalDrafted|" <> show raftLeader <> "|" <> show raftTerm
    StateChangeUpdateCommited {raftLeader, raftTerm} ->
      "StateChangeUpdateCommited|" <> show raftLeader <> "|" <> show raftTerm
    BecameFollower {raftTerm} -> "BecameFollower|" <> show raftTerm
    BecameCandidate {raftTerm} -> "BecameCandidate|" <> show raftTerm
    BecameLeader {raftTerm} -> "BecameLeader|" <> show raftTerm

renderMsg :: Msg input output -> Maybe String
renderMsg (RaftMsg msg) = renderRaftMsg msg
renderMsg _ = Nothing

requestChange :: RaftState input output -> input -> IO ()
requestChange RaftState {raftNode = reqSource, requestVar} reqPayload = do
  reqId <- UUID.nextRandom
  reqTimestamp <- Util.getCurrentTimestamp
  putMVar requestVar
    $ StateChangeReq { reqSource, reqId, reqTimestamp, reqPayload }

requestCurrentState :: DP.SendPort output -> Msg input output
requestCurrentState = RequestCurrentState

requestMsgCount :: DP.SendPort Integer -> Msg input output
requestMsgCount = RequestMsgCount
