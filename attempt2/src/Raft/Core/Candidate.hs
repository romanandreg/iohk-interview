{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE NoImplicitPrelude #-}
module Raft.Core.Candidate ( update ) where

import RIO
import Data.Binary (Binary)
import Control.Distributed.Process (NodeId, Process)
import qualified Control.Distributed.Process as DP

import Raft.Types
import Raft.Util

--------------------------------------------------------------------------------

setNewLeader :: Maybe NodeId -> RaftState input output -> RaftState input output
setNewLeader mleaderId RaftState {..} =
  RaftState { raftRole =
              Follower (FollowerState { raftLeader = mleaderId
                                      , lastInteractionTimestamp = Nothing }), .. }

--------------------------------------------------------------------------------

update
  :: ( Typeable input, Binary input, Show input
     , Typeable output, Binary output, Show output )
  => NodeId
  -> Set NodeId
  -> RaftState input output
  -> RaftMsg input output
  -> (RaftState input output, [Process (RaftMsg input output)])
update myNodeId myPeers raftState msg =
  let
    RaftState {
        raftTerm = myRaftTerm
      , electionLock
      , heartbeatLock
      } = raftState

    onlyMe = null myPeers
    isNodeFromOlderTerm = (myRaftTerm >)
    shouldFollowOtherLeader = (myRaftTerm <)
    ignoreRequest = ( raftState, [] )

  in case msg of
    RaftNoop -> ignoreRequest
    DraftHeartbeat {} -> ignoreRequest
    RegisterLastInteraction {} -> ignoreRequest
    Heartbeat {heartbeatSender, raftTerm}
      | shouldFollowOtherLeader raftTerm ->
        (raftState, [ becomeFollower raftTerm heartbeatSender])
      | otherwise -> ignoreRequest
    CallElection {}
      | onlyMe -> ignoreRequest -- don't want to increase the terms
      | otherwise ->
        ( raftState
          & updateTerm (myRaftTerm + 1)
        , [ startNewElection VoteRequested myNodeId (myRaftTerm + 1) myPeers ] )

    VoteRequested {raftCandidate, raftTerm, voteReplyPort} ->
      let
        replyVote outcome = do
          DP.sendChan voteReplyPort outcome
          return $ VoteReplied raftTerm outcome

      -- I'm an easy candidate, I will follow anyone that runs
      -- on the next term
      in if shouldFollowOtherLeader raftTerm then
        ( raftState
        , [ replyVote True
          , becomeFollower raftTerm raftCandidate
          ]
        )
      else
        ( raftState
        , [ replyVote False
          ]
        )

    VoteReplied {} -> ignoreRequest
    StateChangeProposalRequested {} ->
      -- Someone thinks I'm a leader when I'm a candidate, if the
      -- their term is smaller, it means is an old message and I
      -- can drop it
      ignoreRequest

    StateChangeProposalDrafted {raftLeader, raftTerm, confirmationPort} ->
      -- Old leader talking to me
      if isNodeFromOlderTerm raftTerm then
        ignoreRequest

      -- There is a new sherif in town; become a follower; accept their proposal and
      -- move on
      else
        ( raftState
        , [ becomeFollower raftTerm raftLeader
          , confirmProposal confirmationPort
          ]
        )

    StateChangeUpdateCommited {raftLeader, raftMessageCount, raftTerm, stateChangeReq} ->
      -- Old leader talking to me
      if isNodeFromOlderTerm raftTerm then
        ignoreRequest
      -- There is a new sherif in town; become a follower; accept their proposal and
      -- move on
      else
        ( raftState
          & updateValue stateChangeReq
          & updateMsgCount raftMessageCount
        , [ becomeFollower raftTerm raftLeader ]
        )

    BecameFollower {raftTerm, raftPossibleLeader} ->
      (raftState
        & updateTerm raftTerm
        & setNewLeader raftPossibleLeader

      , [ unlockThread heartbeatLock
        , unlockThread electionLock
        ]
      )

    BecameCandidate {} -> ignoreRequest

    BecameLeader {raftTerm} ->
     (candidateToLeader raftState
       & updateTerm raftTerm
     , [ lockThread electionLock
       , unlockThread heartbeatLock
       ]
     )
