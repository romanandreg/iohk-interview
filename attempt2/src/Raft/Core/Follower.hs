{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE NoImplicitPrelude #-}
module Raft.Core.Follower ( update ) where

import RIO

import Data.Binary (Binary)
import Data.Time.Clock (diffUTCTime)

import Control.Distributed.Process (NodeId, Process)
import qualified Control.Distributed.Process as DP

import Util (getCurrentTimestamp, timestampToUTC)

import Raft.Types
import Raft.Util

--------------------------------------------------------------------------------

-- This variable is used to relax the number of messages sent by heartbeat
-- from the nodes
heartbeatThrottle :: Int
heartbeatThrottle = 10

increaseHeartbeat :: RaftState input output -> RaftState input output
increaseHeartbeat RaftState {heartbeatCount, ..} =
  RaftState { heartbeatCount = (heartbeatCount + 1) `mod` heartbeatThrottle, .. }

followerToCandidate :: FollowerState input output -> RaftState input output -> RaftState input output
followerToCandidate _ raftState =
  raftState { raftRole = Candidate }

setLastInteraction :: Integer -> FollowerState input output -> RaftState input output -> RaftState input output
setLastInteraction timestamp FollowerState {..} RaftState {..} =
  RaftState { raftRole = Follower (FollowerState { lastInteractionTimestamp = Just timestamp, .. })
            , ..
            }

registerLastInteraction :: Process (RaftMsg input output)
registerLastInteraction = do
  ts <- getCurrentTimestamp
  return $ RegisterLastInteraction ts

maybeReleaseRequestVar
  :: StateChangeOp input output
  -> MVar (StateChangeOp input output)
  -> Process (RaftMsg input output)
maybeReleaseRequestVar op requestVar = do
  mreq <- tryReadMVar requestVar
  case mreq of
    -- Don't have any pending requests, so no need to release anything
    Nothing ->
      return ()

    -- This is an invalid implementation
    Just (StateChangeResp {}) ->
      error "ERROR: Having a StateChangeResp in a requestVar; bad sender implementation!"

    -- need to check that the committed response is the one I sent
    Just (StateChangeReq {reqId}) ->
      case op of
        -- This should never happen, considering just failing fast
        StateChangeReq {} ->
          DP.say "ERROR: Received a StateChangeReq on a commit; bad leader implementation!"

        StateChangeResp {reqId = respId}
          | reqId == respId -> void $ takeMVar requestVar
          | otherwise -> return ()

  return RaftNoop

setNewLeader :: Maybe NodeId -> FollowerState input output -> RaftState input output -> RaftState input output
setNewLeader mleaderId FollowerState {..} RaftState {..} =
  RaftState { raftRole = Follower (FollowerState { raftLeader = mleaderId, .. }), .. }

requestStateChangeProposal
  :: (Typeable input, Binary input, Typeable output, Binary output)
  => Maybe NodeId
  -> NodeId
  -> RaftTerm
  -> MVar (StateChangeOp input output)
  -> Process (RaftMsg input output)
requestStateChangeProposal mRaftLeader myNodeId myRaftTerm requestVar  =
  case mRaftLeader of
    Nothing -> do
      DP.say "NOTICE: don't have leader assigned to request a state change; wait till next heartbeat"
      return RaftNoop

    Just raftLeader -> do
      mReq <- tryReadMVar requestVar
      case mReq of
        Just reqPayload -> do
          let req = StateChangeProposalRequested myNodeId myRaftTerm reqPayload
          DP.nsendRemote raftLeader "Raft::Protocol" req
          return req

        Nothing ->
          -- No message available, the next hearbeat may have something for us
          return RaftNoop

--------------------------------------------------------------------------------

update
  :: ( Typeable input, Binary input, Show input
     , Typeable output, Binary output, Show output
     )
  => NodeId
  -> Set NodeId
  -> RaftState input output
  -> FollowerState input output
  -> RaftMsg input output
  -> (RaftState input output, [Process (RaftMsg input output)])
update myNodeId myPeers raftState roleState msg =
  let
    RaftState {
          raftTerm = myRaftTerm
        , electionLock
        , heartbeatLock
        , heartbeatCount
        , requestVar
        } = raftState

    FollowerState {
          raftLeader = myRaftLeader
        , lastInteractionTimestamp
        } = roleState

    lastInteractionTime =
      timestampToUTC <$> lastInteractionTimestamp

    shouldSendProposal = (heartbeatCount + 1) `mod` heartbeatThrottle == 0
    isMyself = (myNodeId == )
    isNodeFromOlderTerm = (myRaftTerm >)
    shouldFollowOtherLeader = (myRaftTerm <)
    dontHaveLeader = isNothing myRaftLeader
    ignoreRequest = ( raftState, [] )
    isMyLeader = (== myRaftLeader) . Just
    becomeCandidate = return $ BecameCandidate (myRaftTerm + 1)
    haveAnyPeers = not $ null myPeers

  in case msg of
    RaftNoop -> ignoreRequest
    DraftHeartbeat {} -> ignoreRequest
    RegisterLastInteraction timestamp ->
      ( setLastInteraction timestamp roleState raftState, [] )

    CallElection {electionTimestamp} ->
      let
        electionTime =
          timestampToUTC electionTimestamp

        mtimeDiff =
          diffUTCTime electionTime <$> lastInteractionTime

      in case mtimeDiff of
        -- When we start the node, we don't have a last interaction
        -- before jumping the gun and calling election, wait for another
        -- timeout election
        Nothing ->
          ( raftState
          , [ registerLastInteraction ]
          )

        Just timeDiff
          | timeDiff < electionTreshold -> ignoreRequest
          | otherwise ->
              ( raftState
              , [ becomeCandidate ]
              )


    Heartbeat {heartbeatSender, raftTerm}
      -- if the sender is not me, and I'm not following a leader, then
      -- I'll follow you
      | not (isMyself heartbeatSender) && dontHaveLeader ->
        ( raftState, [ becomeFollower raftTerm heartbeatSender ] )

      -- if the sender is not me, and the term is higher than mine, we follow a
      -- new leader
      | not (isMyself heartbeatSender) && shouldFollowOtherLeader raftTerm ->
        ( raftState, [ becomeFollower raftTerm heartbeatSender ] )

      -- leader sent a heartbeat, just update last interaction
      | not (isMyself heartbeatSender) ->
        ( raftState, [ registerLastInteraction ] )

      -- I'm doing the heartbeat, which means I will perform a state change proposal
      | otherwise ->
        ( raftState
          & increaseHeartbeat
        , if shouldSendProposal then
            [ requestStateChangeProposal myRaftLeader myNodeId myRaftTerm requestVar ]
          else
            []
        )

    VoteRequested {raftCandidate, raftTerm, voteReplyPort} ->
      let
        replyVote outcome = do
          DP.sendChan voteReplyPort outcome
          return $ VoteReplied raftTerm outcome

      in if shouldFollowOtherLeader raftTerm then
        ( raftState
        , [ replyVote True
          , becomeFollower raftTerm raftCandidate
          ]
        )
      -- vote True if I don't have a leader
      else
        ( raftState, [ replyVote dontHaveLeader ] )

    VoteReplied {} ->
      ignoreRequest

    StateChangeProposalRequested {requestSender, raftTerm}
      -- Someone thinks I'm a leader when I'm a follower, if the
      -- their term is smaller, it means is an old message and I
      -- can drop it
      | isNodeFromOlderTerm raftTerm -> ignoreRequest
      -- I did it, just logging
      | isMyself requestSender -> ignoreRequest
      -- This is bananas, log it
      | otherwise ->
        ( raftState
        , [ do DP.say "ERROR: Received leader message when being a follower. bad implementation!"
               return RaftNoop ]
        )

    StateChangeProposalDrafted { raftLeader, raftTerm, confirmationPort }
      | isNodeFromOlderTerm raftTerm -> ignoreRequest
      | dontHaveLeader ->
        ( raftState, [ becomeFollower raftTerm raftLeader ])
      | shouldFollowOtherLeader raftTerm ->
        ( raftState
        , [ confirmProposal confirmationPort
          , becomeFollower raftTerm raftLeader
          ]
        )
      | isMyLeader raftLeader ->
        ( raftState
        , [ confirmProposal confirmationPort
          , registerLastInteraction
          ]
        )
      -- Bananas, I'm receiving a message from a different leader
      -- on the same term
      | otherwise ->
        ( raftState
        , [
            do DP.say $ "ERROR: Receiving message from different leader on same term "
                      <> show raftLeader <> "/" <> show raftTerm
               return RaftNoop
          ]
        )

    StateChangeUpdateCommited {raftLeader, raftTerm, raftMessageCount, stateChangeReq}
      -- Old leader talking to me
      | isNodeFromOlderTerm raftTerm -> ignoreRequest
      | dontHaveLeader || shouldFollowOtherLeader raftTerm ->
        ( raftState
          & updateValue stateChangeReq
          & updateMsgCount raftMessageCount
        , [ becomeFollower raftTerm raftLeader
          , maybeReleaseRequestVar stateChangeReq requestVar
          ]
        )
      | isMyLeader raftLeader ->
        ( raftState
          & updateValue stateChangeReq
          & updateMsgCount raftMessageCount
        , [ registerLastInteraction
          , maybeReleaseRequestVar stateChangeReq requestVar
          ]
        )
      -- Bananas, I'm receiving a message from a different leader
      -- on the same term
      | otherwise ->
        ( raftState
        , [ -- logMsg raftState msg $ "NOTICE: on older raft term; updating..."
            do DP.say $ "ERROR: Receiving message from more than one leader on same term "
                      <> show raftLeader <> "/" <> show raftTerm
               return RaftNoop
          ]
        )

    BecameFollower {raftTerm, raftPossibleLeader} ->
      let
        raftState1 =
          raftState
          & updateTerm raftTerm
          & setNewLeader raftPossibleLeader roleState
      in
        ( raftState1
        , [ unlockThread heartbeatLock
          , unlockThread electionLock
          , registerLastInteraction
          ]
        )

    BecameCandidate {raftTerm} ->
       ( raftState
         & updateTerm raftTerm
         & followerToCandidate roleState
       , if haveAnyPeers then
           -- I'm the only one running here, I will ignore this
           -- until more people join to avoid going to far on the terms
           [ startNewElection VoteRequested myNodeId raftTerm myPeers
           , lockThread heartbeatLock
           ]
         else
           []
       )

    BecameLeader {} ->
      ( raftState
      , [ do DP.say "ERROR: Received became leader from a follower state, wat?"
             return RaftNoop
        ]
      )
