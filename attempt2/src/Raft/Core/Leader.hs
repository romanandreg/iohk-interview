{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE NoImplicitPrelude #-}
module Raft.Core.Leader ( update ) where

import RIO
import Data.Binary (Binary)

import qualified RIO.Set as Set
import qualified Data.Sequence as Seq

import Control.Distributed.Process (NodeId, Process)
import qualified Control.Distributed.Process as DP

import Raft.Types
import Raft.Util
import Util (hasMajority)

dequeueRequest :: LeaderState input output -> RaftState input output -> RaftState input output
dequeueRequest LeaderState {..} RaftState {..} =
  let
    (knownRequests1, requestQueue1) =
      case Seq.viewl requestQueue of
        Seq.EmptyL -> (knownRequests, Seq.empty)
        req Seq.:< reqs -> (Set.delete (reqId req) knownRequests, reqs)
  in
    RaftState {
        raftRole =
          Leader
          $ LeaderState {
              requestQueue = requestQueue1
            , knownRequests = knownRequests1
            , ..
            }
      , ..
      }

appendRequestChange :: LeaderState input output -> StateChangeOp input output -> RaftState input output -> RaftState input output
appendRequestChange LeaderState {..} req RaftState {..} =
  RaftState
    { raftRole =
        Leader (
            LeaderState { requestQueue = requestQueue Seq.|> req
                        , knownRequests = Set.insert (reqId req) knownRequests
                        , ..
                        }
          )
    , ..
    }

proposeChangeToFollowers
  :: ( Typeable input, Binary input, Show input
     , Typeable output, Binary output, Show output )
  => (NodeId
      -> RaftTerm
      -> StateChangeOp input output
      -> DP.SendPort ()
      -> RaftMsg input output)
  -> NodeId
  -> RaftTerm
  -> RaftState input output
  -> StateChangeOp input output
  -> Set NodeId
  -> Process (RaftMsg input output)
proposeChangeToFollowers mkProposal myNodeId myRaftTerm myState req myPeers = do
    ackCount <- foldM sendChangeProposal initialAckCount myPeers
    if hasMajority ackCount (length myPeers) then do
      DP.say $ "NOTICE: Got majority from my peers"
      let
        RaftState { raftCurrentState, raftMsgCount, raftModifyState } = myState
        resp =
          case req of
            StateChangeReq {..} ->
              StateChangeResp { respPayload = raftModifyState (raftMsgCount, raftCurrentState) reqPayload, .. }
            _ ->
              error $ "Something messed up happened, failing fast " <> show resp

      DP.say $ "NOTICE: Sending change commit to followers " <> show resp
      let msg = StateChangeUpdateCommited myNodeId myRaftTerm (raftMsgCount + 1) resp
      mapM_ (commitChange msg) myPeers
      return msg
    else do
      DP.say "NOTICE: Didn't accomplish majority; attempting at next heartbeat"
      return RaftNoop
  where
    -- includes me ack
    initialAckCount = 1

    commitChange msg peerNode =
      DP.nsendRemote peerNode "Raft::Protocol" msg

    sendChangeProposal :: Int -> NodeId -> Process Int
    sendChangeProposal acc peerNode = do
      (sp, rp) <- DP.newChan
      DP.nsendRemote peerNode "Raft::Protocol" (mkProposal myNodeId myRaftTerm req sp)
      response <- DP.receiveChanTimeout 3000100 rp -- 300 ms
      return $ maybe acc (const $ acc + 1) response

sendHeartbeatToFollowers
  :: ( Typeable input, Binary input
     , Typeable output, Binary output )
  => NodeId
  -> Set NodeId
  -> RaftMsg input output
  -> Process (RaftMsg input output)
sendHeartbeatToFollowers myNodeId peers heartbeatMsg = do
  forM_ (Set.toList peers) $ \peer -> do
    when (peer /= myNodeId) $ DP.nsendRemote peer "Raft::Protocol" heartbeatMsg
  return RaftNoop

setNewLeader :: Maybe NodeId -> RaftState input output -> RaftState input output
setNewLeader mleaderId RaftState {..} =
  RaftState {
      raftRole =
        Follower (
          FollowerState { raftLeader = mleaderId
                        , lastInteractionTimestamp = Nothing}
        )
    , ..
    }

--------------------------------------------------------------------------------

update
  :: ( Typeable input, Binary input, Show input
     , Typeable output, Binary output, Show output
     )
  => NodeId
  -> Set NodeId
  -> RaftState input output
  -> LeaderState input output
  -> RaftMsg input output
  -> (RaftState input output, [Process (RaftMsg input output)])
update myNodeId myPeers raftState roleState msg =
  let
    RaftState {
        raftTerm = myRaftTerm
      , electionLock
      , heartbeatLock
      } = raftState

    LeaderState {
        requestQueue
      , knownRequests
      } = roleState

    isMyself = (myNodeId == )
    isNodeFromOlderTerm = (myRaftTerm >)
    shouldFollowOtherLeader = (myRaftTerm <)
    ignoreRequest = (raftState, [])
    isKnownRequest = (`Set.member` knownRequests) . reqId

  in case msg of
    RaftNoop ->
      ignoreRequest

    DraftHeartbeat {} ->
      ignoreRequest

    RegisterLastInteraction {} ->
      ignoreRequest

    CallElection {} ->
      ignoreRequest

    Heartbeat {heartbeatSender, raftTerm}
      | isNodeFromOlderTerm raftTerm -> ignoreRequest
      | shouldFollowOtherLeader raftTerm ->
         (raftState, [ becomeFollower raftTerm heartbeatSender ])
      | isMyself heartbeatSender ->
        case Seq.viewl requestQueue of
          -- There is no pending request, just send
          -- a heartbeat
          Seq.EmptyL ->
            (raftState
            , [ sendHeartbeatToFollowers myNodeId myPeers msg ]
            )

          -- There is a request in the queue, I'm going to send
          -- the head without removing it, the removal happens when
          -- the change is commited via proposeChangeToFollowers
          pendingReq Seq.:< _rest ->
           ( raftState
           , [ proposeChangeToFollowers
                   StateChangeProposalDrafted
                   myNodeId
                   myRaftTerm
                   raftState
                   pendingReq
                   myPeers
             ]
           )
     | otherwise ->
        error $ "ERROR: Error with election algorithm, more than one leader running "
          <> show (heartbeatSender, raftTerm) <> " " <> show (myNodeId, myRaftTerm)

    VoteRequested {raftCandidate, raftTerm, voteReplyPort} ->
      let
        replyVote outcome = do
          DP.sendChan voteReplyPort outcome
          return $ VoteReplied raftTerm outcome

      in if shouldFollowOtherLeader raftTerm then
        ( raftState
        , [ replyVote True
          , becomeFollower raftTerm raftCandidate
          ]
        )
      else
        ( raftState
        , [ replyVote False ]
        )

    VoteReplied {} ->
      ignoreRequest

    StateChangeProposalRequested {raftTerm, proposedStateChangeReq}
      | isNodeFromOlderTerm raftTerm -> ignoreRequest
      | isKnownRequest proposedStateChangeReq -> ignoreRequest
      | otherwise ->
          ( raftState
            & appendRequestChange roleState proposedStateChangeReq
          , []
          )

    StateChangeProposalDrafted {raftLeader, raftTerm, confirmationPort}
      | isNodeFromOlderTerm raftTerm -> ignoreRequest
      | isMyself raftLeader -> ignoreRequest
      | otherwise ->
        ( raftState
        , [ becomeFollower raftTerm raftLeader
          , confirmProposal confirmationPort
          ]
        )

    StateChangeUpdateCommited {raftMessageCount, raftLeader, raftTerm, stateChangeReq} ->
      let
        raftState1 =
          raftState
          & updateValue stateChangeReq
          & updateMsgCount raftMessageCount
          & dequeueRequest roleState
      in case () of
        _
          | isNodeFromOlderTerm raftTerm -> ignoreRequest
          | shouldFollowOtherLeader raftTerm -> (raftState1, [ becomeFollower raftTerm raftLeader ])
          | otherwise -> (raftState1, [])

    -- NOTE: all pending requests will be gone, and the other nodes will have to
    -- re-send their changes
    BecameFollower {raftTerm, raftPossibleLeader} ->
      let
        raftState1 =
          raftState
          & updateTerm raftTerm
          & setNewLeader raftPossibleLeader
      in
        ( raftState1
        , [ unlockThread electionLock
          , unlockThread heartbeatLock
          ]
        )

    -- These two should not happen
    BecameCandidate {} -> ignoreRequest
    BecameLeader {} -> ignoreRequest
