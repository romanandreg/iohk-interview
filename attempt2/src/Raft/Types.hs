{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE NoImplicitPrelude #-}
module Raft.Types where

import RIO


import Data.Sequence (Seq)
import Data.Time.Clock (NominalDiffTime)
import Data.Binary (Binary)
import Data.UUID (UUID)
import Control.Distributed.Process (NodeId, SendPort)
import qualified System.Random as Random

import Data.Typeable (typeOf)

--------------------------------------------------------------------------------

type RaftTerm = Integer

--------------------------------------------------------------------------------

electionTreshold :: NominalDiffTime
electionTreshold = 2


data StateChangeOp input output
  = StateChangeReq
  { reqSource    :: !NodeId
  , reqId        :: !UUID
  , reqTimestamp :: !Integer
  , reqPayload   :: !input
  }
  | StateChangeResp
  { reqSource    :: !NodeId
  , reqId        :: !UUID
  , reqTimestamp :: !Integer
  , respPayload   :: !output
  }
  deriving (Generic, Typeable, Show)

instance (Binary input, Binary output) => Binary (StateChangeOp input output)

data RaftMsg input output
  = RaftNoop

  -- timer -> follower/candidate
  | CallElection
    {
      electionTimestamp :: !Integer
    }
    -- We need to keep an state that tells us when was the last proposal
    -- received, if the NominalDiffTime is less than the CallElection timeout,
    -- then ignore, otherwise, promote

  | RegisterLastInteraction
    {
      interactionTimestamp :: !Integer
    }

  | DraftHeartbeat { heartbeatTimestamp :: !Integer }

  | Heartbeat
    {
      heartbeatSender    :: !NodeId
    , raftTerm           :: !Integer
    , heartbeatTimestamp :: !Integer
    }
    -- If we have an empty pending state change proposal list, we are
    -- probably going to be demoted

  -- candidate -> follower/candidate/leader
  | VoteRequested
    {
      raftCandidate :: !NodeId
      -- if we vote affirmatively, this is going to become our new leader
    , raftTerm      :: !Integer
      -- if term of message is higher then demote and change leader
    , voteReplyPort :: !(SendPort Bool)
    }
  -- follower/candidate/leader -> candidate
  | VoteReplied
    {
      raftTerm    :: !Integer
      -- debugging purposes
    , voteOutcome :: !Bool
    }

  -- outside client -> follower/leader
  -- | StateChangeProposed
  --   {
  --     proposedStateChangeReq :: !(StateChangeOp input output)
  --   }

  -- follower -> leader
  | StateChangeProposalRequested
    {
      requestSender          :: !NodeId
    , raftTerm               :: !RaftTerm
    , proposedStateChangeReq :: !(StateChangeOp input output)
    }
  -- leader -> follower
  -- A change is being drafted by the leader
  | StateChangeProposalDrafted
    {
      raftLeader       :: !NodeId
    , raftTerm         :: !Integer
    , stateChangeReq   :: !(StateChangeOp input output)
    , confirmationPort :: !(SendPort ())
    }
  -- leader -> follower
  | StateChangeUpdateCommited
    {
      raftLeader       :: !NodeId
    , raftTerm         :: !Integer
    , raftMessageCount :: !Integer
    , stateChangeReq   :: !(StateChangeOp input output)
    }

  -- when receiving a message from term higher than ours
  | BecameFollower
    { raftTerm :: !RaftTerm
    , raftPossibleLeader :: !(Maybe NodeId)
    }
  -- when CallElection happens
  | BecameCandidate
    { raftTerm :: !RaftTerm }
  -- when Majority Vote reached
  | BecameLeader
    { raftTerm :: !RaftTerm }
  deriving (Generic, Typeable, Show)

instance (Binary input, Binary output) => Binary (RaftMsg input output)

data FollowerState input output
  = FollowerState
  { raftLeader               :: !(Maybe NodeId)
  , lastInteractionTimestamp :: !(Maybe Integer)
  }
  deriving (Generic, Typeable, Show)

instance (Binary input, Binary output) => Binary (FollowerState input output)

data LeaderState input output
  = LeaderState
    { requestQueue  :: !(Seq (StateChangeOp input output))
    , knownRequests :: !(Set UUID)
    }
  deriving (Generic, Typeable, Show)

instance (Binary input, Binary output) => Binary (LeaderState input output)

data Role input output
  = Follower !(FollowerState input output)
  | Candidate
  | Leader !(LeaderState input output)
  deriving (Generic, Typeable, Show)

instance (Binary input, Binary output) => Binary (Role input output)

data Msg input output
  = RaftMsg !(RaftMsg input output)
  | RequestCurrentState !(SendPort output)
  | RequestMsgCount !(SendPort Integer)
  | Noop
  deriving (Generic, Typeable, Show)

data RaftDelays
  = RaftDelays
  {
    heartbeatDelayMicros :: Int
  , electionDelayMicros :: Int
  }
  deriving (Generic, Typeable, Show)

data RaftState input output
  = RaftState
  { raftNode         :: !NodeId
  , raftRole         :: !(Role input output)
  , raftCurrentState :: !output
  , raftMsgCount     :: !Integer
  , raftTerm         :: !RaftTerm
  , raftRandomGen    :: !Random.StdGen
  , raftModifyState  :: !((Integer, output) -> input -> output)

  , netListenerLock  :: !(MVar ())
  , electionLock     :: !(MVar ())
  , heartbeatLock    :: !(MVar ())
  , requestVar       :: !(MVar (StateChangeOp input output))
  , heartbeatCount   :: !Int
  , raftDelays        :: !RaftDelays
  }

data RaftArgs input output
  = RaftArgs
  { raftNode          :: !NodeId
  , raftInitialValue  :: !output
  , raftRandomGen     :: !Random.StdGen
  , raftModifyState   :: !((Integer, output) -> input -> output)
  , raftDelays        :: !RaftDelays
  }

-- don't hate me please, need to go fast
instance Typeable a  => Show (MVar a) where
  show m = show $ typeOf m
