{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE NoImplicitPrelude #-}
module Main where

import RIO
import Data.Text.Encoding (decodeUtf8)

import qualified System.Etc as Etc
import Data.Aeson ((.:))
import qualified Data.Aeson as JSON
import qualified Data.Aeson.Types as JSON (Parser)
import qualified Control.Distributed.Process.Node as DPN
import Data.FileEmbed (embedFile)

import qualified Core
import qualified Raft.Core as Raft
import Util

getConfig :: IO Etc.Config
getConfig = do
  configSpec <-
    Etc.parseConfigSpec (decodeUtf8 $(embedFile "resources/config.spec.yml"))

  let configDef = Etc.resolveDefault configSpec
  configCli <- Etc.resolvePlainCli configSpec
  (configFile, _fileWarnings) <- Etc.resolveFiles configSpec
  configEnv <- Etc.resolveEnv configSpec

  return (configDef <> configEnv <> configCli <> configFile)

delayParser :: JSON.Value -> JSON.Parser Raft.RaftDelays
delayParser = JSON.withObject "RaftDelays" $ \object ->
  Raft.RaftDelays
    <$> object .: "heartbeatDelayMicros"
    <*> object .: "electionDelayMicros"

main :: IO ()
main = do
  config <- getConfig

  -- Etc.printPrettyConfig config

  raftDelays     <- Etc.getConfigValueWith delayParser ["raft"] config
  host           <- Etc.getConfigValue ["host"] config
  (port :: Int)  <- Etc.getConfigValue ["port"] config
  otherHosts     <- Etc.getConfigValue ["other-hosts"] config
  withSeed       <- Etc.getConfigValue ["with-seed"] config
  sendForSeconds <- Etc.getConfigValue ["send-for"] config
  waitForSeconds <- Etc.getConfigValue ["wait-for"] config

  let portStr = show port
      fullHost = host <> ":" <> portStr
      otherHosts1 = filter (/= fullHost) otherHosts

  let
    startSystem =
      Core.main
        host
        (show port)
        DPN.initRemoteTable
        (map makeNodeId otherHosts1)
        raftDelays
        withSeed
        sendForSeconds
        waitForSeconds
        -- (do threadDelay 100000
        --     runDistributedSystem withSeed sendForSeconds waitForSeconds)

  startSystem
